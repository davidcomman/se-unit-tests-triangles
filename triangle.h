#include <string>
#include <vector>
#include <iostream>


#ifndef SE_TRIANGLES_TRIANGLE_H
#define SE_TRIANGLES_TRIANGLE_H


class triangle {
public:
    triangle(std::vector<int> angles);
    std::string get_type();
private:
    float angles[3];
};


#endif //SE_TRIANGLES_TRIANGLE_H
