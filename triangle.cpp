#include <stdexcept>
#include "triangle.h"

triangle::triangle(std::vector<int> angles) {
    int angles_given = angles.size();
    if(angles_given != 3) throw std::invalid_argument("The triangle class expects 3 angles, not " + std::to_string(angles_given));

    int sum = 0;
    for(int i = 0; i < 3; i++) {
        sum += angles.at(i);
        this->angles[i] = angles.at(i);
    }
    if(sum != 180) throw std::domain_error("The triangle class expects the sum of all angles to be 180, not " + std::to_string(sum));
}

std::string triangle::get_type() {
    int equal_amt = 1;
    if(angles[0] == angles[1]) equal_amt++;
    if(angles[0] == angles[2]) equal_amt++;

    switch(equal_amt) {
        case 1: return "Scalene";
        case 2: return "Isosceles";
        case 3: return "Equilateral";
    }
    return "";
}