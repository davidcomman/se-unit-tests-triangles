#include <iostream>
#include "triangle.h"

int main() {
    std::vector<int> angles = {59, 59, 62};
    triangle t(angles);

    std::cout << t.get_type() << std::endl;
    return 0;
}