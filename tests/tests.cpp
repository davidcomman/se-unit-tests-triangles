
#include <gtest/gtest.h>
#include "../triangle.h"
#include "../triangle.cpp"

TEST(triangle_tests, instantiate_object) {
    std::vector<int> v1 = {30, 90, 60};
    std::vector<int> v2 = {30, 90};
    std::vector<int> v3 = {30, 100, 60};
    ASSERT_NO_THROW(triangle t(v1));
    ASSERT_THROW(triangle t(v2), std::invalid_argument);
    ASSERT_THROW(triangle t(v3), std::domain_error);
}

TEST(triangle_tests, boundary_value_analysis) {
    std::vector<int> v1 = {60, 60, 60};
    std::vector<int> v2 = {59, 61, 60};
    std::vector<int> v3 = {59, 59, 62};

    triangle t1(v1);
    triangle t2(v2);
    triangle t3(v3);

    ASSERT_STREQ(t1.get_type().c_str(), "Equilateral");
    ASSERT_STREQ(t2.get_type().c_str(), "Scalene");
    ASSERT_STREQ(t3.get_type().c_str(), "Isosceles");
}